<?php

namespace App\Http\Controllers;

use App\Listing;
use Auth;
use Illuminate\Http\Request;

class ListingController extends Controller
{
  
    public function index()
    {
        $list=Listing::all();
        $data=compact('list');
        return view('listing.manage-listing',$data);
    }

 
    public function create()
    {
        return view('listing.add-listing');
    }

    
    public function store(Request $request)
    {
        $list=new Listing;
        $list->list_name=$request->name;
        $list->address=$request->address;
        $list->latitude=$request->latitude;
        $list->longitude=$request->longitude;
        $list->submitter_id=Auth::user()->id;
        $list->save();
        return redirect()->route('listing.index');
    }

    public function show(Listing $listing)
    {
        //
    }

  
    public function edit(Listing $listing)
    {
        $data=compact('listing');
        return view('listing.edit-listing',$data);
    }

    public function update(Request $request, Listing $listing)
    {

        $listing->list_name=$request->name;
        $listing->address=$request->address;
        $listing->latitude=$request->latitude;
        $listing->longitude=$request->longitude;
        $listing->submitter_id=Auth::user()->id;
        $listing->save();
        return redirect()->route('listing.index');
    }

  
    public function destroy(Listing $listing)
    {
        $listing->delete();
        return redirect()->route('listing.index');
    }
    public function apilisting(Request $request){
        $listing =Listing::where('submitter_id',$request->user_id)->get();
      foreach($listing as $list) {
        $list->distance = number_format($this->distance($request->latitude,$request->longitude,$list->latitude,$list->longitude),3);
      }
      return response()->json(['listing'=>$listing,'status'=>['code'=>200,'message'=>'Listing successfully retrieved']]);
    }
    
    function distance($lat1, $lon1, $lat2, $lon2) {
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
          return 0;
        }
        else {
          $theta = $lon1 - $lon2;
          $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
          $dist = acos($dist);
          $dist = rad2deg($dist);
          $miles = $dist * 60 * 1.1515;
            return ($miles * 1.609344);
        
        }
      }
}
