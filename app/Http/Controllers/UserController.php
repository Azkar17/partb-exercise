<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user =User::all();
        $data=compact('user');
        return view('admindashboard.manage-user',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admindashboard.add-user');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user =new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->type = $request->type;
        $user->save();
      return redirect()->route('user.index');
    }

    
    public function show(User $user)
    {
        //
    }

   
    public function edit(User $user)
    {
        $data=compact('user');
        return view('admindashboard.edit-user',$data);
    }

    
    public function update(Request $request, User $user)
    {
        $user->name=$request->name;
        $user->email=$request->email;
        $user->type=$request->type;
        $user->save();
        return redirect()->route('user.index');
    }

   
    public function destroy(User $user)
    {
       $user->delete();
       return redirect()->route('user.index');

     
    }
}
