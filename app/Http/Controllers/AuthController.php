<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Listing;
use Illuminate\Support\Facades\Auth;


class AuthController extends Controller
{
    public function login(){ 
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
           $user = Auth::user(); 
           $token['token'] =  $user->createToken('PartB')-> accessToken; 
            return response()->json(['user_id'=>$user->id,'token'=>$token['token'] ,'status'=>['code'=> 200 ,'message'=>'Access Granted']]); 
          } else{ 
           return response()->json(['error'=>'Unauthorised'], 401); 
           } 
        }

   
  
}
