@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header"> Admin Dashboard -> Create User</div>

                <div class="card-body ">
                <form  method="POST"  action="{{route('user.store')}}"> 
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-10 ">
                            <label>Name</label>
                            <input type="text" name="name" class="form-control" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-10">
                            <label>Email</label>
                            <input type="email" name="email" class="form-control" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-10">
                            <label>Password</label>
                            <input type="password" name="password" class="form-control" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-10">
                            <label>Select User Type</label>
                            <select  name="type" class="form-control" required>
                                <option value="u">user </option>
                                <option value="a">admin </option>
                            </select>
                             
                        </div>
                    </div>
                    <button class="btn btn-primary mt-2 float-right" type="submit">Create User</button>
                    </form>

                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
