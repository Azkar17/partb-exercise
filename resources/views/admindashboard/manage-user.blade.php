@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
            <div class="card-header"> Admin Dashboard -> Manage User  <a class="btn btn-primary float-right" href="{{route('user.create')}}">Add User</a></div>

                <div class="card-body ">
                <table class="table">
                    <thead>
                        <th>User Id</th>
                        <th>User Name</th>
                        <th>User Email</th>
                        <th>User Type </th>
                        <th >Action</th>
                    </thead>
                    @forelse ($user as $item)
                    <tbody>   
                    <td>{{$item->id}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{$item->email}}</td>
                            <td>@if ($item->type == 'u')
                                {{'user'}}
                            @else
                                {{'admin'}}
                            @endif </td>
                            <td >
                                <div class="row"><a href="{{route('user.edit',$item->id)}}" class="btn btn-warning">Edit</a>&nbsp;
                                <form action="{{route('user.destroy',$item->id)}}" method="POST">
                                        @method('DELETE')
                                        @csrf
                                        <button class="btn btn-danger">Delete</button>
                                    </form></div>
                               </td>                
                  </tbody>
                            @empty
                            <td colspan="8"> No data</td>
                            @endforelse

                </table>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
