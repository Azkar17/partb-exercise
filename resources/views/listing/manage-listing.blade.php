@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
            <div class="card-header"> List Dashboard -> Manage List  <a class="btn btn-primary float-right" href="{{route('listing.create')}}">Add List</a></div>

                <div class="card-body ">
                <table class="table">
                    <thead>
                        <th>List Id</th>
                        <th>List Name</th>
                        <th>List Address</th>
                        <th>Latitude </th>
                        <th >Longititude</th>
                        <th >Submitter_id</th>
                        <th>Action</th>
                    </thead>
                    @forelse ($list as $item)
                    <tbody>   
                    <td>{{$item->id}}</td>
                    <td>{{$item->list_name}}</td>
                    <td>{{$item->address}}</td>
                    <td>{{$item->latitude }}</td>
                    <td>{{$item->longitude}}</td>
                    <td>{{$item->submitter_id}}</td>        
                    <td >
                                <div class="row"><a href="{{route('listing.edit',$item->id)}}" class="btn btn-warning">Edit</a>&nbsp;
                                <form action="{{route('listing.destroy',$item->id)}}" method="POST">
                                        @method('DELETE')
                                        @csrf
                                        <button class="btn btn-danger">Delete</button>
                                    </form></div>
                               </td>                
                  </tbody>
                            @empty
                            <td colspan="8"> No data</td>
                            @endforelse

                </table>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
