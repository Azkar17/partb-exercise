@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header"> List Dashboard -> Edit List</div>

                <div class="card-body ">
                <form  method="POST"  action="{{route('listing.update',$listing->id)}}"> 
                        @method('PATCH')
                    {{ csrf_field() }}
                    <div class="row">
                            <div class="col-md-10 ">
                                <label> List Name</label>
                            <input type="text" name="name" value="{{$listing->list_name}}" class="form-control" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10">
                                <label>List Address</label>
                            <input type="text" name="address" value="{{$listing->address}}" class="form-control" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10">
                                <label>Latitude</label>
                            <input type="text" name="latitude"  value="{{$listing->latitude}}" class="form-control" required>
                            </div>
                        </div>
                        <div class="row">
                                <div class="col-md-10">
                                    <label>Longitude</label>
                                    <input type="text" name="longitude" value="{{$listing->longitude}}" class="form-control" required>
                                </div>
                            </div>
                        <button class="btn btn-primary mt-2 float-right" type="submit">Update List</button>
                        </form>
    

                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
